"""System commands."""

from typing import Any


def _quit_close(mudpy_instance: Any, command: str) -> None:  # pylint: disable=unused-argument
    """Quit and close the browser."""
    raise SystemExit


def _disconnect(mudpy_instance: Any, command: str) -> None:  # pylint: disable=unused-argument
    """Disconnect the client."""
    if mudpy_instance.client:
        mudpy_instance.client.close()
        mudpy_instance.client = None


def _reconnect(mudpy_instance: Any, command: str) -> None:  # pylint: disable=unused-argument
    """Reconnect the client."""
    mudpy_instance.receive()
