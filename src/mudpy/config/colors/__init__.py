from .colors import TAGS
from .colors import TERMINAL_COLORS
from .colors import add_custom_tags
from .colors import highlight_custom_tags
from .colors import add_colors
