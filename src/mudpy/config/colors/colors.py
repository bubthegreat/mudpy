"""Functions to set and control colors in mudpy."""

import logging
import re
import tkinter as tk
from typing import Any

from mudpy.config import CONFIG


LOGGER = logging.getLogger(__name__)
TERMINAL_COLORS = list(CONFIG["colormap"]["terminal"].keys())


class Tag:
    """Tag object for managing tags easier."""

    # pylint: disable=too-few-public-methods

    def __init__(self, match_string: str, tag_name: str, color: str, regexp: bool) -> None:
        """Initialize a tag object."""
        self.match_string: str = match_string
        self.tag_name: str = tag_name
        self.color: str = color
        self.regexp: bool = regexp


TAGS = [
    Tag("(Hide)", "hidden", "purple", False),
    Tag(r".*\w+ has arrived.", "arrival", "#ffff00", True),
    Tag(r"^(\w+\s?)+<.*>", "prompt", "#ffffff", True),
    Tag("(PK)", "pk", "red", False),
]


def add_custom_tags(mudpy_instance: Any) -> None:
    """Add all the custom tag colors."""
    for tag in TAGS:
        mudpy_instance.text_box.tag_config(tag.tag_name, foreground=tag.color, background="black")
        LOGGER.info("Initialized tag %s with color %s", tag.tag_name, tag.color)


def highlight_pattern(mudpy_instance: Any, tag: Tag, start: str = "1.0", end: str = "end") -> None:
    """Apply the given tag to all text that matches the given pattern

    If 'regexp' is set to True, pattern will be treated as a regular
    expression according to Tcl's regular expression syntax.
    """

    start = mudpy_instance.text_box.index(start)
    end = mudpy_instance.text_box.index(end)
    mudpy_instance.text_box.mark_set("matchStart", start)
    mudpy_instance.text_box.mark_set("matchEnd", start)
    mudpy_instance.text_box.mark_set("searchLimit", end)

    count = tk.IntVar()
    while True:
        index = mudpy_instance.text_box.search(
            tag.match_string, "matchEnd", "searchLimit", count=count, regexp=tag.regexp
        )
        if index == "":
            break
        if count.get() == 0:  # type: ignore
            break  # degenerate pattern which matches zero-length strings
        mudpy_instance.text_box.mark_set("matchStart", index)
        mudpy_instance.text_box.mark_set("matchEnd", "%s+%sc" % (index, count.get()))  # type: ignore
        mudpy_instance.text_box.tag_add(tag.tag_name, "matchStart", "matchEnd")


def highlight_custom_tags(mudpy_instance: Any, current_end_index: str, new_end_index: str) -> None:
    """Highlight any custom tags between two indexes."""
    for tag in TAGS:
        highlight_pattern(
            mudpy_instance, tag, start=current_end_index, end=new_end_index,
        )


def add_colors(message: str) -> str:
    """Add user desired colors to messages."""
    message = re.sub(r"\(PK\)", "\x1b[31;1m(PK)\x1b[0m", message)  # Make PK red
    message = re.sub(r"IMM", "\x1b[36;1mIMM\x1b[0m", message)  # Make IMM cyan
    message = re.sub(r"You feel better!", "\x1b[33;1mYou feel better!\x1b[0m", message)  # Make IMM cyan

    return message
