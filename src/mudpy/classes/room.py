import logging
import re

LOGGER = logging.getLogger(__name__)

class Room:
    def __init__(self, lines):
        LOGGER.debug("Initialized room with lines: %s", lines)
        self._lines = lines
        self._processed = False
        self.is_room = False
        self.appending_desc = True
        self.room_exits_line = ''
        self.title = ''
        self.prompt = ''
        self._description = []
        self._other_room_lines = []
        self.exits = []

        self.items = []
        self.mobs = []

        self.process_raw_lines()
        self.process_other_lines()

    def __str__(self):
        return self.title

    def __repr__(self):
        return f"<Room({self.title})>"

    @property
    def description(self):
        return ' '.join(self._description)

    def process_raw_lines(self):
        for index, line in enumerate(self._lines):
            if not line.strip():
                continue
            if 'exits' in line.lower():
                LOGGER.info("You're in a room.")
                self.is_room = True
                self.room_exits_line = line
                self.appending_desc = False
            elif self.appending_desc:
                LOGGER.info("Added another line to the description: %s", line)
                self._description.append(line)
            else:
                LOGGER.info("Added another 'other' line: %s", line)
                self._other_room_lines.append(line)
        if self.is_room and self._description:
            LOGGER.info("Pulling room title from Room description")
            self.title = self._description.pop(0)

    def process_other_lines(self):
        if self._other_room_lines:
            LOGGER.info("Popping prompt off of other room lines.")
            self.prompt = self._other_room_lines.pop(-1)
        for line in self._other_room_lines:
            if re.match('\s{4,}', line):
                LOGGER.debug("Found item match in line: %s", line)
                self.items.append(line.strip())
            else:
                LOGGER.debug("Found mob match in line: %s", line)
                self.mobs.append(line.strip())

        exit_match = re.search(r".*Exits: (?P<exit_str>.*)]", self.room_exits_line)
        if exit_match:
            LOGGER.info("Found exits match in line %s", self.room_exits_line)
            exit_str = exit_match.group('exit_str')
            if exit_str:
                self.exits = exit_str.split()
