import nltk
import time

import logging
import re
import time
import threading, queue
import telnetlib
import nltk
from mudpy.classes.room import Room



nltk.download('punkt')
nltk.download('averaged_perceptron_tagger')
nltk.download('maxent_ne_chunker')
nltk.download('words')

LOGGER = logging.getLogger(__name__)

class CFClient:


    def __init__(self):
        self.client = None

        self.current_room = None
        self._last_message = None
        self._previous_message = None
        self.new_message = True

        self.iq = queue.Queue()
        self.oq = queue.Queue()
        self.cq = queue.Queue()

        # turn-on the worker thread
        self.t1 = threading.Thread(target=self.worker, daemon=True)
        self.t1.start()

        self.t2 = threading.Thread(target=self.consumer, daemon=True)
        self.t2.start()

        self.t3 = threading.Thread(target=self.producer, daemon=True)
        self.t3.start()

    def set_client(self, host='localhost', port=9999):
        self.client = telnetlib.Telnet(host, port)
        LOGGER.info(f"Client is set to {host}:{port}")

    @property
    def last_message(self):
        self.new_message = False
        LOGGER.debug("New message set to false.")
        return self._last_message

    @last_message.setter
    def last_message(self, message):
        LOGGER.debug("Got a new message.")
        self._previous_message = self._last_message
        self._last_message = message
        self.new_message = True


    def send(self, cmd):
        cmd += '\n'
        self.client.write(cmd.encode("ascii"))
        LOGGER.info("Wrote command %s", cmd)

    def look(self, target=None, wait=True, timeout=3):
        self.new_message = False
        if not target:
            LOGGER.debug("Looking.")
            self.send('look')
        else:
            LOGGER.debug(f"Looking at {target}")
            self.send(f'look {target}')
        self.wait_for_message(timeout=timeout)
        LOGGER.debug('last message: %s', self.last_message)


    def set_new_client(self, client):
        LOGGER.info("Setting new client.")
        self.client = client

    def wait_for_message(self, timeout=3):
        LOGGER.info(f"Waiting for message for {timeout} seconds.")
        start_time = time.time()
        elapsed_time = time.time() - start_time

        while True:
            if elapsed_time > timeout:
                LOGGER.error(f"Didn't get an answer in {timeout} seconds")
                LOGGER.debug('last message: %s', self.last_message)
                break
            if not self.new_message:
                time.sleep(0.01)
            else:
                LOGGER.debug(f"Received a new message after {elapsed_time} seconds.")
                break
            elapsed_time = time.time() - start_time

    def examine(self, target=None, look=False, excluded={'a', 'ring', 'north', 'south', 'east', 'west', 'up', 'down'}):
        self.look(target=target)
        excluded = {word.lower() for word in excluded}
        LOGGER.info("Examining")
        if target:
            LOGGER.info("Target %s will be examined.")
            tokens = nltk.word_tokenize(' '.join(self.last_message[:-1]))
        else:
            LOGGER.info("No target given - current room description.")
            tokens = nltk.word_tokenize(self.current_room.description)
        tagged = nltk.pos_tag(tokens)

        nouns = set()
        for word, pos in tagged:
            lowered = word.lower()
            if (pos == 'NN' or pos == 'NNP' or pos == 'NNS' or pos == 'NNPS'):
                if lowered in excluded:
                    continue
                nouns.add(lowered)
        LOGGER.info(f"Found nouns: {nouns}")
        if not look:
            return

        found_nouns = set()
        for noun in nouns:
            LOGGER.debug(f"Looking for {noun}")
            self.look(noun)
            last_message_str = ' '.join(self.last_message)
            if 'You do not see that here' in last_message_str:
                continue
            if 'and weighs' in last_message_str and 'worn on' in last_message_str and 'made of' in last_message_str:
                continue

            LOGGER.debug(self.last_message)
            found_nouns.add(noun)
            print(noun)
        LOGGER.info("Returning found nouns.")
        return found_nouns


    def report_stuff(self, target=None, look=True, excluded={'a', 'ring', 'north', 'south', 'east', 'west', 'up', 'down'}, sayit=False):
        LOGGER.info("Attempting to report stuff.")
        all_found = set()
        found_nouns = self.examine(target=target, look=look, excluded=excluded)
        for noun in found_nouns:
            LOGGER.info("Examining additional found nouns.")
            all_found.add(noun)
            more_found = self.examine(target=noun, look=True)
            for thing in more_found:
                LOGGER.info("Foun additional thing to look at: %s", thing)
                all_found.add(thing)

        if sayit:
            LOGGER.info("Oh he gon' say it..")
            if len(all_found) == 1:
                self.send(f"say You might find a {', '.join(found_nouns)} interesting.")
            if len(all_found) > 1:
                fn_list = list(all_found)
                first = fn_list[:-1]
                last = fn_list[-1]
                self.send(f"say You might find a {', '.join(first)} and {last} interesting.")
            else:
                self.send("say I see nothing interesting here.")


    def producer(self):
        if not self.client:
            self.set_client()

        while True:
            raw_message = self.client.read_very_eager()
            if not raw_message:
                time.sleep(0.1)
                continue
            message = raw_message.decode('ascii')
            message = message.replace("\r\n", "\n")
            message = message.replace("\n\r", "\n")
            self.iq.put(message)
            LOGGER.debug("Put message on input queue: %s", message)
            self.iq.task_done()


    def worker(self):
        chars = ''
        start = time.time()
        while True:
            message = self.iq.get()
            now = time.time()
            elapsed = now - start
            if elapsed > 0.25:
                self.oq.put(chars)
                chars = ''
            for char in message:
                if char == '\n':
                    self.oq.put(chars)
                    chars = ''
                    continue
                chars += char
            self.oq.put(chars)
            chars = ''
            self.oq.task_done()


    def consumer(self):
        message = []
        while True:
            line = self.oq.get()
            message.append(line)
            match = re.search("\w+\s+<.*>", line)
            if not match:
                LOGGER.debug('added another line to message (%s): %s', len(message), line)
            if match:
                LOGGER.info("Found prompt")
                room = Room(message)
                if room.is_room:
                    self.current_room = room
                    LOGGER.debug("You're in a room.")
                self.last_message = message
                LOGGER.debug("Setting last message: %s", message)
                message = []