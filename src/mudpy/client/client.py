"""Main client classes."""

import logging
import re
import socket
import telnetlib
import time
import tkinter as tk
import queue
from threading import Thread
from typing import Optional, List, Any
from mudpy.config import load_config
from mudpy.config.colors import add_colors, TERMINAL_COLORS, highlight_custom_tags
from mudpy.config.gui import configure_mudpy_gui
from mudpy.commands import handle_command

COLOR_TAG = r"\x1b\[(?P<color>\d+);?(?P<bright>\d+)?m"
COLOR_TAG_REG = re.compile(COLOR_TAG)
DEFAULT_TEXT_COLOR = "lime green"

LOGGER = logging.getLogger(__name__)


class MudPyClient:
    """General class for mudpy."""

    # pylint: disable=too-many-instance-attributes

    def __init__(self, add_controller: bool = True) -> None:
        """Initialize a mudpy instance."""
        LOGGER.info("Initializing MudPy GUI")
        self.config = load_config()
        self.controller: Optional[socket.socket] = None
        self.app = tk.Tk()
        self.menubar = tk.Menu(self.app)
        self.my_msg = tk.StringVar()
        self.scrollbar = tk.Scrollbar(self.app)
        self.text_box = tk.Text(self.app, yscrollcommand=self.scrollbar.set, font=("Courier", 13))
        self.scrollbar.config(command=self.text_box.yview)
        self.entry_field = tk.Entry(self.app, textvariable=self.my_msg)
        self.receiving = True
        self.add_controller = add_controller
        self._set_file_menu()
        self.client: Optional[telnetlib.Telnet] = None
        self.history: List[str] = []
        self.history_index = 0
        self.listening = False
        self.rqueue = queue.Queue()
        configure_mudpy_gui(self)

        LOGGER.info("Initialization complete.  Ready to MUD like a motherfucker.")
        self.receive()

    def _set_file_menu(self) -> None:
        """Configure your magical file menu."""
        filemenu = tk.Menu(self.menubar, tearoff=0)
        filemenu.add_command(label="Connect", command=self.receive, underline=0)  # type: ignore
        filemenu.add_command(label="Reconnect", command=self.reconnect, underline=0)  # type: ignore
        filemenu.add_command(label="Disconnect", command=self.disconnect, underline=0)  # type: ignore
        filemenu.add_command(  # type: ignore
            label="Save Log", command=lambda: LOGGER.info("Save log called."), underline=0  # type: ignore
        )  # type: ignore
        filemenu.add_separator()  # type: ignore
        filemenu.add_command(label="Quit", command=self.quit, underline=0)  # type: ignore
        self.menubar.add_cascade(label="File", menu=filemenu, underline=0)  # type: ignore
        self.app.config(menu=self.menubar)

    def reconnect(self) -> None:
        """Reconnect to the server."""
        LOGGER.info("reconnecting.")
        self.disconnect()
        self.receive()

    def process_message(self, message: str) -> None:
        """Process a message and emit to the GUI."""
        # normalize the new line calls so we can work with
        # then consistently.
        current_end_index = self.text_box.index("end")  # type: ignore
        message = message.replace("\r\n", "\n")
        message = message.replace("\n\r", "\n")

        # Add addional color tags to the ones that we already
        # get from the server.
        # message = add_colors(message)
        LOGGER.debug("Message: %s", repr(message))

        # Find all the places where we have color tags.
        mylist = list(re.finditer(COLOR_TAG, message))

        current_color = "0"
        last_index = 0
        # tkinter uses tags for color in the GUI, so we have to
        # iterate through the messages and color tags and add those
        # color tags in the right places.
        for match in mylist:
            if match.group("color"):
                next_color = match.group("color")
                if next_color not in TERMINAL_COLORS:
                    LOGGER.error("No color tag for: %s", next_color)
            else:
                next_color = "0"
            # 90% sure that I can clean this up and reduce a lot of this
            # logic, but it's working for now.
            current_text = message[last_index : match.start()]
            cleaned_text = COLOR_TAG_REG.sub("", current_text)
            last_index = match.end()
            self.text_box.insert(tk.END, cleaned_text, current_color)  # type: ignore
            if self.controller is not None:
                self.controller.sendall(cleaned_text.encode())
            current_color = next_color

        # Get the rest of the message at whatever the current color is and send it
        # to the GUI
        current_text = message[last_index:]
        cleaned_text = COLOR_TAG_REG.sub("", current_text)
        self.text_box.insert(tk.END, cleaned_text, current_color)  # type: ignore
        new_end_index = self.text_box.index("end")  # type: ignore
        LOGGER.debug("Processed lines from : %s to %s", current_end_index, new_end_index)
        highlight_custom_tags(self, current_end_index, new_end_index)
        if self.controller is not None:
            self.controller.sendall(cleaned_text.encode())

    def disconnect(self) -> None:
        """Disconnect from the server."""
        LOGGER.info("Disconnecting client.")
        if self.client:
            self.client.close()

    def _receive(self, host: str = "carrionfields.net", port: int = 4449) -> None:
        """
        Handle receiving of messages.

        The receive method is a threaded call that shares memory with the main thread.
        This threading call should be constantly checking the socket buffers to see
        if there are any new messages, processing the messages, and updating the GUI.
        """
        LOGGER.info("Connecting to server")
        self.client = telnetlib.Telnet(host, port)
        self.receiving = True
        while self.receiving:
            # This is arbitrary, but is just here to stop the receive function from
            # going full blast.
            # NOTE: Fix this to a config driven thing.
            time.sleep(0.05)
            text_messages = []
            read_msg = True
            # Receive is always going to be receiving as long as read_msg is true.  The
            # only time we shouldn't be receiving is when we've been disconnected.  This
            # will be useful for offline things later, but admittedly is implemented in
            # a pretty hacky way.
            while read_msg:
                if not self.client:
                    return
                # https://gitlab.com/bubthegreat/mudpy/-/issues/1
                # Implement this as a coroutine so we're processing it as it comes in
                # rather than waiting for all of the buffer to be read in before we do any
                # processing.
                try:
                    # Read eager will just get whatever is in the buffer.  If there's nothing
                    # in the buffer it'll break from the message read and try again.
                    msg = self.client.read_very_eager()
                    # If there's no message in the buffer we need to wait a little longer
                    # and check again.
                    if not msg:
                        read_msg = False
                        break
                # EOFError happens when the socket connection is gone.
                except EOFError:
                    self.text_box.insert("end", "You were disconnected.")  # type: ignore
                    self.receiving = False
                    read_msg = False
                    return
                # Reads a
                text_messages.append(msg.decode("ascii"))
            # If we have any messages, bring them all together in a string and process them.
            # This is hacky and simple, but should be updated to just process whatever chunk
            # of the buffer we got with the read_eager() call.
            if text_messages:
                full_message = "".join(text_messages)
                # If there's nothing but empty characters then there's
                # nothing to see here, move along.
                if not full_message.strip():
                    continue
                self.process_message(full_message)
                self.text_box.see("end")  # type: ignore

    def send(self, event: tk.Event = None, byte_message: bytes = None) -> None:  # pylint: disable=unused-argument
        """Handle sending of messages."""
        # get all the text in textbox
        if byte_message:
            msg: str = byte_message.decode()
        else:
            msg: str = self.my_msg.get()  # type: ignore
            if msg.strip():
                self.history.append(msg)
                self.history_index = len(self.history) - 1
        msg: str = byte_message.decode() if byte_message else self.my_msg.get()  # type: ignore
        for message in msg.split(";"):
            exec_message = message + "\n" if "\n" not in message else message
            # Write the ascii encoded message to the server
            self.text_box.insert(tk.END, exec_message.encode("ascii"), "33")  # type: ignore
            if not self.client:
                return
            executed = handle_command(self, msg)
            if executed:
                return
            self.client.write(exec_message.encode("ascii"))
            LOGGER.info("Sent message to server: %s", msg)
            # Send a copy of the sent output to the GUI
            # Go to the end of the text box
            # https://gitlab.com/bubthegreat/mudpy/-/issues/3
            # This will need to be enabled/disabled when clicking and dragging
            # on the scrollbar
            if not byte_message:
                self.text_box.see("end")  # type: ignore
                # Select all the entry field text so if you type something else
                # it overwrites - this is a convenience factor that's huge.if not byte_message:
                self.entry_field.select_range(0, tk.END)

    def receive(self) -> None:
        """Run the main logic of the mudpy gui."""
        LOGGER.info("Main receive thread started.")
        # Separate thread for receiving.  Threading being used
        # so we don't need to implement queues that have poorer performance vs.
        # the thread memory sharing.  We're not doing anything that's not network
        # constrained, so the GIL shouldn't be a blocker here, but if it does end
        # up being problematic, we'll need to look at implementing queues and multi
        # process instead of threading.
        # https://gitlab.com/bubthegreat/mudpy/-/issues/5
        # Should investigate doing this in asyncio
        receive_thread = Thread(target=self._receive)
        receive_thread.setDaemon(True)
        receive_thread.start()
        LOGGER.info("Completed starting main receive thread.")
        if self.add_controller and not self.listening:
            LOGGER.info("Adding controller listener")
            listen_thread = Thread(target=self.listen)
            listen_thread.setDaemon(True)
            listen_thread.start()
            self.listening = True

    def listen(self) -> None:
        """Listen for a controller to connect."""
        host = "localhost"  # Standard loopback interface address (localhost)
        port = 9999  # Port to listen on (non-privileged ports are > 1023)
        LOGGER.info("Starting listener socket on %s:%s.", host, port)
        while self.listening:
            try:
                with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
                    LOGGER.info("Started listener.")
                    sock.bind((host, port))
                    sock.listen()
                    conn, addr = sock.accept()
                    self.controller = conn
                    if self.controller is not None:
                        with self.controller:
                            LOGGER.info("Controller connected: %s", addr)
                            while True:
                                data = self.controller.recv(1024)
                                if not data:
                                    break
                                LOGGER.info("Controller sent: %s", data)
                                self.send(byte_message=data)
                    sock.close()
            # Keep going unless we explicitly stop listening.
            except:
                LOGGER.exception("Encountered an error with the listener socket.")
                pass

    def quit(self) -> None:
        """Quit the application."""
        LOGGER.info("Quitting.")
        self.app.destroy()
        self.app.quit()  # type: ignore
