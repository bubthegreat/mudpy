# MudPy

I've played MUD games for most of my life, and was tired of seeing unmaintained or ones that weren't actually multi-platform, or relied on so many unique dependencies that they were impossible to install.  MudPy is a pure Python implementation of a mud client.

The project is still in active development, but once it's at a stage where it's worth releasing I'll get it put on pypy.

![MudPy](/media/screenshot.png)

# Install and run

1. `git clone https://gitlab.com/bubthegreat/mudpy.git`
2. `pip install --user . `
3. `mudpy`

# Contributing
The only requirements in the requirements file are for testing and development.  We use pre-commit to make sure things stay clean, typed, and pythonic.

Please contribute from a fork!

1. `git clone https://gitlab.com/bubthegreat/mudpy.git`
2. `pip install -r requirements.txt`
3. `pre-commit install`
